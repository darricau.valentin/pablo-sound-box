import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { NativeAudio } from 'ionic-native';
import {AboutPage} from "../about/about";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  songs = [
    new Song('comamierda', 'assets/audio/comamierda.mp3', 'Coma Mierda'),
    new Song('marica', 'assets/audio/marica.mp3', 'Marica'),
    new Song('tomar', 'assets/audio/tomar.mp3', 'Tomar'),
    new Song('queputa', 'assets/audio/queputa.mp3', 'Que Puta'),
    new Song('cojones', 'assets/audio/cojones.mp3', 'Cojones'),
    new Song('cali', 'assets/audio/cali.mp3', 'Cali'),
    new Song('castanos', 'assets/audio/castanos.mp3', 'Castaños'),
    new Song('sipatronsolo', 'assets/audio/sipatronsolo.mp3', 'Si Patrón'),
    new Song('muerte', 'assets/audio/muerte.mp3', 'Muerte'),
    new Song('elcartel', 'assets/audio/elcartel.mp3', 'El Cartel'),
    new Song('hijitos', 'assets/audio/hijitos.mp3', 'Hijitos'),
    new Song('hijodeputa', 'assets/audio/hijodeputa.mp3', 'Hijo de Puta'),
    new Song('judy', 'assets/audio/judy.mp3', 'Judy'),
    new Song('mensaje', 'assets/audio/mensaje.mp3', 'Mensaje'),
    new Song('plata', 'assets/audio/plata.mp3', 'Plata'),
    new Song('posey', 'assets/audio/posey.mp3', 'Posey'),
    new Song('entrar', 'assets/audio/entrar.mp3', 'Entrar'),
    new Song('laugh', 'assets/audio/laugh.mp3', 'Risa'),
  ];

  pabloFaceSong = new Song('yosoy', 'assets/audio/yosoy.mp3', 'Yo Soy');

  constructor(public navCtrl: NavController) {
    for (let song of this.songs) {
      song.preloadSimple();
    }
    this.pabloFaceSong.preloadSimple();
  }

  play(songPlayed: Song){
    for (let song of this.songs) {
      song.stop();
    }
    this.pabloFaceSong.stop();
    songPlayed.play();
  }

  aboutPage = AboutPage;

  pabloFaces = [
    new PabloFace(0, 'assets/img/pablo_face.svg'),
    new PabloFace(1, 'assets/img/pablo_face1.svg'),
    new PabloFace(2, 'assets/img/pablo_face2.svg'),
  ];

  pabloFaceImage = this.pabloFaces[0];
  changePabloFace(index){
    for (let pabloFace of this.pabloFaces) {
      if (pabloFace.id == (index + 1)) {
        this.pabloFaceImage = pabloFace;
        this.play(this.pabloFaceSong);
        return;
      }
    }
    this.play(this.pabloFaceSong);
    this.pabloFaceImage = this.pabloFaces[0];
  }
}


class PabloFace {
  constructor(public id: number, public src: string){

  }
}

class Song{
  constructor(public uniqueId: string, public path: string, public name: string){

  }

  preloadSimple(){
    NativeAudio.preloadSimple(this.uniqueId, this.path)
      .then(() => console.log('success loading ' + this.name), (msg) => console.log(msg));
  }

  play(){
    NativeAudio.play(this.uniqueId).then(() => console.log('success playing music ' + this.name), (msg) => console.log(msg));
  }

  stop(){
    NativeAudio.stop(this.uniqueId).then(() => console.log('success stopping music ' + this.name), (msg) => console.log(msg));
  }
}
